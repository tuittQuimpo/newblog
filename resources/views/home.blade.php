@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading text-center">Congratulations, {{ Auth::user()->name }}! You are now a member of City Hoops!</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="centerize">
                        <a href="/"><button class="btn btn-primary" style="margin: 0 auto;"> Go back Home </button></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
