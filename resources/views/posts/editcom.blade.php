@extends('layout')

@section ('content')

<div class="col-md-8">

	<h1>Edit this Comment</h1>

		<div class="card">

		  <div class="card-body">

		   <form method="POST">

		   	{{ csrf_field() }}
		   	
		   	<div class="form-group">
		   		<textarea name="body" placeholder="Your comment here." class="form-control" required>{{$comment_tbe->body}}</textarea>
		   	</div>

		   	<div class="form-group">
			  	<button type="submit" class="btn btn-primary">Submit New Comment</button>
			 </div>

		   </form>
		   

		   @include ('layouts.errors')

		  </div>

		</div>

</div>


@endsection