@extends ('layout')

@section ('content')

 <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

	<div class="col-sm-8 blog-main">

		<h1 id="gametitle">{{ $post->title }}</h1>
		<p>Posted by: <strong>{{ $post->user->name }}</strong></p>

		@if (count($post->tags))

				
				@foreach ($post->tags as $tag)

						
						<a href="/posts/tags/{{ $tag->name }}">

							{{ $tag->name }}

						</a>



				@endforeach


		@endif

		{{ $post->body }}

		{{ $post->date }}

		  @if ( Auth::user()->id == $post->user_id )

		  	<a href='{{url("posts/$post->id/delete")}}'><button class="btn btn-danger" name="delete">Delete</button></a>

		  	<a href='{{url("posts/$post->id/edit")}}'><button class="btn btn-info" name="edit">Edit</button></a>

		  @endif

		<hr>


		<div class="comments">

			<ul class="list-group">
				@foreach ($post->comments as $comment)

				
					<li class="list-group-item">
						
						<strong>

							{{ $comment->user->name }}:

						</strong>

						<small>
							
								( {{ $comment->created_at->diffForHumans() }} ):

						</small>

						<br>

						{{ $comment->body }} &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp 

					@if ( Auth::user()->id == $comment->user_id )

		  				<a href='{{url("comments/$comment->id/delete")}}'><button class="btn btn-danger" name="delete">Delete</button></a>

		  				<a href='{{url("comments/$comment->id/edit")}}'><button class="btn btn-info">Edit</button></a>

		 		    @endif


				@endforeach



					</li>

			</ul>

		</div>

		<hr>

		<!-- add a comment -->

		<div class="card">

		  <div class="card-body">

		   <form method="POST" action="/posts/{{ $post->id }}/comments">

		   	{{ csrf_field() }}
		   	
		   	<div class="form-group">
		   		<textarea name="body" placeholder="Your comment here." class="form-control" required></textarea>
		   	</div>

		   	<div class="form-group">
			  	<button type="submit" class="btn btn-primary">Add a Comment</button>
			 </div>

		   </form>
		   

		   @include ('layouts.errors')

		  </div>

		</div>



	</div>


@endsection