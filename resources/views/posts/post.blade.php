 
 <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <div class="container blog-post">

    <div class="row" id="gameb">

      <div class="col-md-12">
      
        <a href="/posts/{{ $post->id }}">
           
          <h2 class="blog-post-title text-center" id="gamename">
                  
             {{ $post->title }}

          </h2>

        </a>

      </div>


      <div class="col-md-6 col-xs-6 col-sm-6 text-center" id="gamedetails">

        <h6> {{ $post->date }} </h6>

      </div>      

      <div class="col-md-6 col-xs-6 col-sm-6 text-center" id="gamedetails">

        <h6> {{ $post->user->name }} </h6>

      </div>

    </div> <!-- endrowgame -->

    <div class="row" id="venued">

      <div class="col-md-2 col-xs-2 col-sm-2" id="venuetaglo">
        
        <img src="{{ URL::asset('logo.png') }}" height="40" width="40" class="img-responsive">

      </div>
      
      <div class="col-md-10 col-xs-10 col-sm-10" id="venuetag">
        
        <h2>    @if (count($post->tags))

            <ul id="tagul">
              
              @foreach ($post->tags as $tag)

                <li>
                  
                  <a href="/posts/tags/{{ $tag->name }}">

                    {{ $tag->name }}

                  </a>

                </li>

              @endforeach

            </ul>

              @endif

        </h2>

      </div>

    </div>

      <div class="row" id="gamebodyd">

        <div class="col-md-2 col-xs-2 col-sm-2" id="starpic">
          
            <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>

        </div>


        <div class="col-md-10 col-xs-10 col-sm-10" id="gamebody">
          
            <strong> {{ $post->body }}  </strong> <br style="height: 4px;">

            <small>Game hostedby: {{ $post->user->name }} </small>

        </div>

      </div> <!-- endrow -->
            
  </div><!-- /.blog-post -->

  <hr>