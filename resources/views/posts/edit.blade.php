@extends('layout')

@section ('content')

<div class="col-sm-8">

	<h1>Edit this Post</h1>

	<hr>

	<form method="POST">

		{{ csrf_field() }}

	  <div class="form-group">
	    <label for="title">Title:</label>
	    <input type="text" class="form-control" id="title" name="title" value="{{$post_tbe->title}}">
	  </div>

	  <div class="form-group">
	    <label for="body">Body</label>
	    <textarea id="body" name="body" class="form-control">{{$post_tbe->body}}</textarea>
	  </div>

	  <div class="form-group">
	  	<label for="date">Date:</label>
	  	<input type="date" name="date" class="datepicker" value="{{$post_tbe->date}}">
	  </div>

	  <div class="form-group">
	  	<button type="submit" class="btn btn-primary">Publish Edit</button>
	  </div>
	 

	 	@include ('layouts.errors')

	</form>




</div>



@endsection