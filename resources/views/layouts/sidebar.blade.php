<aside class="col-sm-3 ml-sm-auto blog-sidebar">
  
          <div class="sidebar-module sidebar-module-inset text-center">
          </div>

          <div class="sidebar-module card">
            <h4 class="text-center">Upcoming Games</h4>
            <ol class="list-unstyled list-group list-group-flush">

              @foreach ($archives as $stats)

                <li class="list-group-item text-center">
                  
                  <a href="/?month={{ $stats['month'] }}&year={{ $stats['year'] }}">

                    {{ $stats['month'] . ' ' . $stats['year'] }}

                  </a>

                </li>

              @endforeach

            </ol>

          </div>


          <div class="sidebar-module sidebar-module-inset">
         
          <div class="sidebar-module card">
            <h4 class="text-center">Search by Gym location</h4>
            <ol class="list-unstyled list-group list-group-flush">

              @foreach ($tags as $tag)

                <li class="list-group-item text-center">
                  
                  <a href="/posts/tags/{{ $tag }}">

                    {{ $tag }}

                  </a>

                </li>

              @endforeach

            </ol>

          </div>






          <div class="sidebar-module">
            <h4>Elsewhere</h4>
            <ol class="list-unstyled">
              <li><a href="#">GitHub</a></li>
              <li><a href="#">Twitter</a></li>
              <li><a href="#">Facebook</a></li>
            </ol>
          </div>
</aside><!-- /.blog-sidebar -->