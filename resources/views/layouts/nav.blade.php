<div class="pos-f-t">

  <div class="collapse" id="navbarToggleExternalContent">

    <div class="bg-dark text-left p-2">

                        @guest
                            <li class="d-inline nav-link"><a href="/login">| Login |</a></li>
                            <li class="d-inline nav-link"><a href="/register">| Register |</a></li>
                        @else
                            <li class="d-inline nav-link"><a href="/">| Timeline |</a></li>
                            <li class="d-inline nav-link"><a href="/posts/create">| Create Game |</a></li>
                            <li class="dropdown d-inline nav-link" style="list-style: none;">
                                <a href="/" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest


    </div>

  </div>

  <nav class="navbar navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">


      <span class="navbar-toggler-icon"></span>

    </button>

            <img src="{{URL::asset('chLogo')}}" alt="profile Pic" height="40" width="120" class="img-responsive">


<!--  -->


  </nav>
</div>