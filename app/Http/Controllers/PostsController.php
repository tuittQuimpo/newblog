<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;

use App\Repositories\Posts;

use Carbon\Carbon;

use Auth;




class PostsController extends Controller

{
    public function __construct()

    {

        $this->middleware('auth')->except(['index', 'show']);

    }


    public function index()

    {

        // return $tag->posts;


    $posts = Post::latest()

    ->filter(request()->only(['month', 'year']))

    ->get();




    return view('posts.index', compact('posts'));

    }



    public function show(Post $post)

    {

    	return view('posts.show', compact('post'));
    }
	
	public function create()

    {
    	return view('posts.create');
    }
	
	public function store()

    {
    	$post = new Post;

    	// $post->title = request('title');

	// $post->body = request('body');

    	// $post->save();

        $this->validate(request(),[

            'title' => 'required',

            'body' => 'required',

            'date' => 'required'


        ]);


        auth()->user()->publish(

            new Post(request([ 'title', 'body', 'date' ]))
            
        );


        session()->flash(

            'message', 'You have created a new game!'

        );


    return redirect('/');


    	// Post::create([

     //        'title' => request('title'),

     //        'body' => request('body'),

     //        'date' => request('date'),

     //        'user_id' => auth()->id()

     //        // dd(auth()->id())

     //    ]);

 
    }

    function delete($id)

    {
    
        $article_to_delete = \App\Post::find($id);
        
        $article_to_delete->delete();

        return redirect('/');

    }


    function edit_form($id) {
        $post_tbe = Post::find($id);

        return view('posts.edit',
            compact('post_tbe'));
    }

    function edit($id, Request $request, Post $post) {

        $post_tbe = Post::find($id);
        $post_tbe->title = $request->title;
        $post_tbe->body = $request->body;
        $post_tbe->date = $request->date;
        $post_tbe->save();


        return redirect("/");
    }

}
