<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

use App\Post;

use App\Comment;

use Auth;

class CommentsController extends Controller
{

    public function store(Post $post)

    {

    	$this->validate(request(), ['body' => 'required|min:2']);
    	
    	$user_id = Auth::user()->id;

        $post->addComment(request('body'),$user_id);




    	return back();

    }

    function delete($id)

    {
    	$comment_to_delete = \App\Comment::find($id);
    	
    	$comment_to_delete->delete();

    	return "Your comment has been deleted";
    }

        function edit_comments_form($id)
    {

        $comment_tbe = Comment::find($id);


        return view ('posts.editcom',

            compact('comment_tbe'));
    }

    function edit_comments($id, Request $request, Post $post) {

        $comment_tbe = Comment::find($id);

        $comment_tbe->body = $request->body;

        $comment_tbe->save();


        return redirect("/");

    }

}
