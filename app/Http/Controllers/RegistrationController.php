<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Mail\Welcome;


class RegistrationController extends Controller
{
    public function create()

    {

    	return view('registration.create');


    }

    public function store()

    {   
        


    	// validate the form

    	$this->validate(request(), [

    		'username' => 'required',
    		
    		'name' => 'required',

    		'email' => 'required|email',

    		'password' => 'required|confirmed'
    	]);


    	//create and save the user

    	$user = User::create([

        'name'=>request('name'),

        'username'=>request('username'),

        'email'=>request('email'),

        'password'=>bcrypt(request('password'))

        ]);

    	//sign them in

    	auth()->login($user);

        \Mail::to($user)->send(new Welcome);


        session()->flash('message', 'Thanks so much for signing up to City Hoops!');

    	//redirect to the home page

    	return redirect()->index('/');


    }

}
