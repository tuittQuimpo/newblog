<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{

    use DatabaseTransactions;


    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->get(true);

        //given

        //and each one is posted a month apart
        $first = factory(Post::class)->create();

        $second = factory(Post::class)->create([
            'created_at' => Carbon\Carbon:now()->subMonth()
        ]);

        //when
        $posts = Post::archives();

        //then
        $this->assertEquals([

            [

                "year" => $first->created_at->format('Y'),
                "month" => $first->created_at->format('F'),
                "published" => 1

            ]

[

                "year" => $second->created_at->format('Y'),
                "month" => $second->created_at->format('F'),
                "published" => 1

            ]


        ], $posts);

        
    }
}
