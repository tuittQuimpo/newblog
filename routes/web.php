<?php






Route::get('/', 'PostsController@index')->name('home');

Route::get('/posts/create', 'PostsController@create');

Route::post('/posts', 'PostsController@store');

Route::get('/posts/{id}/delete', 'PostsController@delete');

Route::get('posts/{id}/edit', 'PostsController@edit_form');

Route::post('posts/{id}/edit',  'PostsController@edit');


Route::get('/posts/{post}', 'PostsController@show');



Route::get('/posts/tags/{tag}', 'TagsController@index');


 
Route::post('/posts/{post}/comments', 'CommentsController@store');

Route::get('/comments/{id}/delete', 'CommentsController@delete');

Route::get('/comments/{id}/edit', 'CommentsController@edit_comments_form');

Route::post('/comments/{id}/edit', 'CommentsController@edit_comments');

Auth::routes();

// Route::get('/home', 'HomeController@index');

Route::post('/register', 'RegistrationController@store');

Route::get('/register', 'RegistrationController@create');

Route::get('/login', 'SessionsController@create');

Route::post('/login', 'SessionsController@store');

Route::get('/logout', 'SessionsController@destroy');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
